package dwi.bagas.survey

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name, null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tSoal = "create table soal(id_soal integer primary key autoincrement, soal text not null)"
        val tJawab = "create table jawab(nomor text primary key, nama text not null, id_soal int not null, jawab text not null)"
        val insSoal = "insert into soal(soal) values('Apakah anda pernah sakit Demam Berdarah dan sampai di rawat dirumah sakit?'),('Apakah anda pernah sakit Tifus dan sampai di rawat dirumah sakit?'),('Apakah anda pernah Patah Tulang dan sampai dioperasi?')"
        db?.execSQL(tSoal)
        db?.execSQL(tJawab)
        db?.execSQL(insSoal)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "soal"
        val DB_Ver = 1
    }
}
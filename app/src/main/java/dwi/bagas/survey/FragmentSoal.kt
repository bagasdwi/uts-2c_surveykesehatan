package dwi.bagas.survey

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_soal.view.*

class FragmentSoal : Fragment(),View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnDelete ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnUpdate ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin Ingin Mengubah Data?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }

        }

    }

    lateinit var  thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var  builder: AlertDialog.Builder
    var idSoal : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_soal,container, false)
        v.btnUpdate.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsSoal.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataSoal(){
        val cursor : Cursor = db.query("soal", arrayOf("soal", "id_soal as _id"),
            null, null, null, null, "_id asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_soal,cursor,
            arrayOf("_id","soal"), intArrayOf(R.id.txIdSoal, R.id.txSoal),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsSoal.adapter = adapter
    }

    override fun onStart(){
        super.onStart()
        showDataSoal()
    }
    val itemClick = AdapterView.OnItemClickListener {parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idSoal = c.getString(c.getColumnIndex("_id"))
        v.edSoal.setText(c.getString(c.getColumnIndex("soal")))
    }

    fun insertDataSoal(soal : String){
        var cv : ContentValues = ContentValues()
        cv.put("soal",soal)
        db.insert("soal", null,cv)
        showDataSoal()
    }

    fun updateDataSoal (soal : String, idSoal : String){
        var cv : ContentValues = ContentValues()
        cv.put("soal",soal)
        db.update("soal",cv,"id_soal = $idSoal",null)
        showDataSoal()
    }

    fun deleteDataSoal(idSoal: String){
        db.delete("soal","id_soal = $idSoal", null)
        showDataSoal()
    }

    val btnInsertDialog = DialogInterface.OnClickListener {dialog, which ->
        insertDataSoal(v.edSoal.text.toString())
        v.edSoal.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener {dialog, which ->
        deleteDataSoal(idSoal)
        v.edSoal.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener {dialog, which ->
        updateDataSoal(v.edSoal.text.toString(),idSoal)
        v.edSoal.setText("")
    }
}
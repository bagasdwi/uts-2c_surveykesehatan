package dwi.bagas.survey

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_jawab.*
import kotlinx.android.synthetic.main.frag_data_jawab.view.*

class FragmentJawab : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        soal = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnInsertJawab -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah Anda Yakin?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
//            R.id.btnCari -> {
//                showDataJawab(edNamaJawab.text.toString())
//            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaJawab : String=""
    var soal : String=""
//    var id_soal : String = ""
    var arrSoal = ArrayList<String>()
    lateinit var  db : SQLiteDatabase


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_jawab,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnInsertJawab.setOnClickListener(this)

        v.spinner.onItemSelectedListener = soalSelect
        return v
    }
    override fun onStart() {
        super.onStart()
        showDataJawab()
        showDataSoal()
    }
    val soalSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = spAdapter.getItem(position) as Cursor
            soal = c.getString(c.getColumnIndex("_id"))
        }
    }

    fun showDataJawab(){
        var sql="select j.nomor as _id, j.nama as nama2, s.soal as soal2, j.jawab as jawab2 from jawab j, soal s " +
                "where j.id_soal=s.id_soal order by j.nomor asc "

        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_jawab,c,
            arrayOf("_id","nama2","soal2","jawab2"), intArrayOf(R.id.txNomorJawab,R.id.txNamaJawab,R.id.txNamaSoal,R.id.txJawab),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsJawab.adapter = lsAdapter
    }

    fun showDataSoal() {
        val c: Cursor = db.rawQuery("select soal as _id from soal order by soal asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrSoal.add(temp) //add the item
            c.moveToNext()
        }
    }

        fun insertDataJawab(nomor: String, namaJawab: String, id_soal: Int, jawab: String) {
            var sql = "insert into jawab(nomor, nama, id_soal, jawab) values(?,?,?,?)"
            db.execSQL(sql, arrayOf(nomor, namaJawab, id_soal, jawab))
            showDataJawab()
        }

        val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
            var sql = "select id_soal from soal where soal='$soal'"
            val c: Cursor = db.rawQuery(sql, null)
            if (c.count > 0) {
                c.moveToFirst()
                insertDataJawab(
                    v.edNomorJawab.text.toString(), v.edNamaJawab.text.toString(),
                    c.getInt(c.getColumnIndex("id_soal")),
                    v.edJawab.text.toString()
                )
            }
            v.edNomorJawab.setText("")
            v.edNamaJawab.setText("")
            v.spinner.setSelection(0)
            v.edJawab.setText("")
            }
        }


